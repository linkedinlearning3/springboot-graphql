package phat.springbootgraphqldemo.controller;

import jakarta.persistence.Column;
import lombok.Getter;
import lombok.Setter;
import phat.springbootgraphqldemo.entity.Customer;

@Getter
@Setter
public class CustomerInput {
    private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;
    private String address;
    private String city;
    private String state;
    private String zipCode;

    public Customer getCustomerEntity() {
        Customer customer = new Customer();
        customer.setFirstName(this.getFirstName());
        customer.setLastName(this.getLastName());
        customer.setEmail(this.getEmail());
        customer.setPhoneNumber(this.getPhoneNumber());
        customer.setAddress(this.getAddress());
        customer.setCity(this.getCity());
        customer.setState(this.getState());
        customer.setZipCode(this.getZipCode());
        return customer;
    }
}
