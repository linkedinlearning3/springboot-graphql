package phat.springbootgraphqldemo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import phat.springbootgraphqldemo.entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
    Customer findCustomerByEmail(String email);
}
