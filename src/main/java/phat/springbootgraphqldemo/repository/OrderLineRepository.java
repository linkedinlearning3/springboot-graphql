package phat.springbootgraphqldemo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import phat.springbootgraphqldemo.entity.OrderLine;

public interface OrderLineRepository extends JpaRepository<OrderLine, Long> {
}
