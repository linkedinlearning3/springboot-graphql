package phat.springbootgraphqldemo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import phat.springbootgraphqldemo.entity.Product;

public interface ProductRepository extends JpaRepository<Product, String> {
}