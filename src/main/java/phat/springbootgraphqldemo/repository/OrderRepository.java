package phat.springbootgraphqldemo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import phat.springbootgraphqldemo.entity.Order;

public interface OrderRepository extends JpaRepository<Order, String> {
}