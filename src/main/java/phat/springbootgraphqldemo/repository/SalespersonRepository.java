package phat.springbootgraphqldemo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import phat.springbootgraphqldemo.entity.Salesperson;

public interface SalespersonRepository extends JpaRepository<Salesperson, Long> {
    Salesperson findSalespersonByEmail(String email);
}